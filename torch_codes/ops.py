import torch 
import numpy as np 

if __name__ == '__main__':
    x = torch.arange(6).reshape(3, 2)
    print(x)
    print(x[:, 1:])
    type(x[1, 1])

    x = torch.arange(10)
    x = x.reshape(2, 5)
    print(x)
    print(x.view(-1, 2))

    a = torch.Tensor([1, 2, 3])
    b = torch.Tensor([4, 3, 2]) #  Notice 'T'

    print(a + b)
    print(a.mul(b))
    print(a)
    print(a.mul_(b))
    print(a)

    a = torch.Tensor([1, 2, 3])
    b = torch.Tensor([4, 3, 2])

    print(a * b)
    print(a.dot(b))

    a = torch.Tensor([[0, 2, 4], [1, 3, 5]])
    b = torch.Tensor([[6, 7], [8, 1], [0, 1]])

    torch.mm(a, b)
    a@b

    x = torch.Tensor([3, 4, 5])
    x.norm()
    print(x.numel())
