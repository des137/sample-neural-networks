import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data
import torch.nn.functional as F
import torchvision
from torchvision import transforms
from PIL import Image
    
class SimpleNet(nn.Module):
    
    def __init__(self):
        super(SimpleNet, self).__init__()
        self.fc1 = nn.Linear(64*64*3, 84)
        self.fc2 = nn.Linear(84, 50)
        self.fc3 = nn.Linear(50, 2)
    
    def forward(self, x):
        x = x.view(-1, 64*64*3)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x
    
class CNNNet(nn.Module):

	def __init__(self, num_classes=2):
		super(CNNNet, self).__init__()
		self.features = nn.Sequential(
			nn.Conv2d(3, 64, kernel_size=11, stride=4, padding=2),
			nn.ReLU(),
			nn.MaxPool2d(kernel_size=3, stride=2),
			nn.Conv2d(64, 192, kernel_size=5, padding=2),
			nn.ReLU(),
			nn.MaxPool2d(kernel_size=3, stride=2),
			nn.Conv2d(192, 384, kernel_size=3, padding=1),
			nn.ReLU(),
			nn.Conv2d(384, 256, kernel_size=3, padding=1),
			nn.ReLU(),
			nn.Conv2d(256, 256, kernel_size=3, padding=1),
			nn.ReLU(),
			nn.MaxPool2d(kernel_size=3, stride=2),
		)
		self.avgpool = nn.AdaptiveAvgPool2d((6, 6))
		self.classifier = nn.Sequential(
			nn.Dropout(),
			nn.Linear(256*6*6, 4096),
			nn.ReLU(),
			nn.Dropout(),
			nn.Linear(4096, 4096),
			nn.ReLU(),
			nn.Linear(4096, num_classes)
		)

	def forward(self, x):
		x = self.features(x)
		x = self.avgpool(x)
		x = torch.flatten(x, 1)
		x = self.classifier(x)
		return x

def check_image(path):
    try:
        im = Image.open(path)
        return True
    except:
        return False

def select_device():
    if torch.cuda.is_available():
        device = torch.device("cuda") 
    else:
        device = torch.device("cpu")
    return device

def image_transformer():
    img_transforms = transforms.Compose([
        transforms.Resize((64, 64)),
        transforms.ToTensor(), 
        transforms.Normalize(mean=[0.485, 0.456, 0.406], 
                             std=[0.229, 0.224, 0.225])
    ])
    return img_transforms

def load_data(img_transforms, batch_size=64):

    train_data_path = './train/'
    val_data_path = "./val/"
    test_data_path = "./test/"
    
    train_data = torchvision.datasets.ImageFolder(root=train_data_path, 
        transform=img_transforms, is_valid_file=check_image)
    val_data = torchvision.datasets.ImageFolder(root=val_data_path, 
        transform=img_transforms, is_valid_file=check_image)
    test_data = torchvision.datasets.ImageFolder(root=test_data_path, 
        transform=img_transforms, is_valid_file=check_image)

    train_data_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, shuffle=True)
    val_data_loader = torch.utils.data.DataLoader(val_data, batch_size=batch_size, shuffle=True)
    test_data_loader = torch.utils.data.DataLoader(test_data, batch_size=batch_size, shuffle=True)

    return train_data_loader, val_data_loader, test_data_loader


def train(model, optimizer, loss_fn, train_loader, val_loader, epochs=20, device="cpu"):
    for epoch in range(epochs):
        training_loss = 0.0
        valid_loss = 0.0
        model.train()
        for batch in train_loader:
            optimizer.zero_grad()
            inputs, targets = batch
            inputs = inputs.to(device)
            targets = targets.to(device)
            output = model(inputs)
            loss = loss_fn(output, targets)
            loss.backward()
            optimizer.step()
            training_loss += loss.data.item() * inputs.size(0)
        training_loss /= len(train_loader.dataset)
        
        model.eval()
        num_correct = 0 
        num_examples = 0
        for batch in val_loader:
            inputs, targets = batch
            inputs = inputs.to(device)
            output = model(inputs)
            targets = targets.to(device)
            loss = loss_fn(output,targets) 
            valid_loss += loss.data.item() * inputs.size(0)
            correct = torch.eq(torch.max(F.softmax(output), 
                dim=1)[1], targets).view(-1)
            num_correct += torch.sum(correct).item()
            num_examples += correct.shape[0]
        valid_loss /= len(val_loader.dataset)

        print('Epoch: {}, Training Loss: {:.2f}, Validt Loss: {:.2f}, accuracy = {:.2f}'.\
            format(epoch, training_loss,
        valid_loss, num_correct / num_examples))

def prediction_test(img_transforms, device, model):
    labels = ['cat','fish']

    img = Image.open("./val/fish/100_1422.JPG") 
    img = img_transforms(img)[None].to(device)

    prediction = F.softmax(model(img))
    prediction = prediction.argmax()
    return labels[prediction]

def save_model(model, model_path):
	torch.save(model, model_path)
	return 

def load_model(model_path):
	model = torch.load(model_path)	
	return model

def save_model_dict(model, model_path):
	torch.save(model.state_dict(), model_path)
	return

def load_model_dict(model_path):
	model = torch.load(model_path)
	return model

def main():
    img_transforms = image_transformer()
    train_data_loader, val_data_loader, test_data_loader = load_data(img_transforms)

    neural_net = CNNNet()#SimpleNet()
    optimizer_ = optim.Adam(neural_net.parameters(), lr=0.001)

    device = select_device()
    neural_net.to(device)
    train(neural_net, optimizer_, nn.CrossEntropyLoss(), 
        train_data_loader, val_data_loader, epochs=10)

    print(prediction_test(img_transforms, device, neural_net))

    model_path = "./tmp_model/cnnnet"#"./tmp_model/simplenet"
    save_model(neural_net, model_path)
    neural_net = load_model(model_path)
    
    save_model_dict(neural_net, model_path)
    neural_net = SimpleNet()
    neural_net_state_dict = load_model_dict(model_path)
    #neural_net.load_state_dict(neural_net_state_dict)

if __name__ == '__main__':
    main()