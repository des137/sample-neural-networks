import torch 
import numpy as np

if __name__ == '__main__':
    print(torch.__version__)
    arr = np.array([1, 2, 3, 4, 5])
    print(arr)
    print(arr.dtype)

    x = torch.from_numpy(arr)
    print(x)
    y = torch.as_tensor(arr)
    print(y)

    z = torch.tensor(arr) # Deep copy
    print(z)

    z2 = torch.tensor(np.arange(10).reshape(2, 5))
    print(z2)
    print(z2.dtype)
    z2n = z2.type(torch.int32)
    print(z2n.dtype)

    z3 = torch.Tensor(arr) # Converts to float
    print(z3)

    torch.empty(2, 2)
    torch.linspace(0, 1, 11)
    torch.arange(0, 18, 2).reshape(3, 3)
    torch.zeros(2, 2, dtype=torch.float64)
    torch.ones(2, 2)

    torch.rand(4, 3)
    torch.randn(4, 3)

    torch.rand_like(z3)

    torch.manual_seed(1)
