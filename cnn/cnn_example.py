# Keras on Cifar10 data

import numpy as np
from keras.utils import to_categorical
from keras.datasets import cifar10
from keras.models import Model
from keras.layers import (Input, Conv2D, BatchNormalization, LeakyReLU, 
    Flatten, Dense, Dropout, Activation)
from keras.optimizers import Adam
from keras.utils import plot_model

(x_train, y_train), (x_test, y_test) = cifar10.load_data()

NUM_CLASSES = 10
x_train = x_train.astype('float32')/255.0
x_test = x_test.astype('float32')/255.0

y_train = to_categorical(y_train, NUM_CLASSES)
y_test = to_categorical(y_test, NUM_CLASSES)


input_layer = Input(shape=(32, 32, 3))

x = Conv2D(filters=32, kernel_size=3, strides=1, padding='same')(input_layer)
x = BatchNormalization()(x)
x = LeakyReLU()(x)

x = Conv2D(filters=32, kernel_size=3, strides=2, padding='same')(x)
x = BatchNormalization()(x)
x = LeakyReLU()(x)

x = Conv2D(filters=64, kernel_size=3, strides=1, padding='same')(x)
x = BatchNormalization()(x)
x = LeakyReLU()(x)

x = Conv2D(filters=64, kernel_size=3, strides=2, padding='same')(x)
x = BatchNormalization()(x)
x = LeakyReLU()(x)

x = Flatten()(x)

x = Dense(128)(x)
x = BatchNormalization()(x)
x = LeakyReLU()(x)
x = Dropout(rate=0.5)(x)

x = Dense(NUM_CLASSES)(x)
output_layer = Activation('softmax')(x)
model = Model(input_layer, output_layer)

opt = Adam(lr=5*10**-4)
model.compile(loss='categorical_crossentropy', optimizer=opt, 
    metrics=['accuracy'])
model.fit(x_train, y_train, batch_size=32, epochs=10, shuffle=True)
model.evaluate(x_test, y_test)

plot_model(model)
